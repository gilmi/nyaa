{- | Animetime

We'd like to have a place that shows which anime episodes recently
came out and give a link to a torrent with a clean, nice interface

We'll try to query nyaa for that.

What we are interested in for each anime is

- it's title
- it's image

For each episode, we'd like to know

- Which anime is it from
- What is the episode number
- Give a link to a torrent


---

For the UI, we'd like to:

- Show new releases and download links to anime
- Show pictures for each anime
- Three modes for anime:
>- Mark favorite to highlight
>- Normal is displayed but not highlighted
>- Mark hide for not displayed anime

-}

module Animetime where

import Data.Foreign.Index as FI
import Text.Parsing.Simple as P
import Control.Alternative ((<|>))
import Control.Comonad (extract)
import Control.Monad.Aff (Aff, attempt, launchAff)
import Control.Monad.Aff.Console (log)
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE)
import Control.Monad.Eff.Exception (EXCEPTION)
import Control.Monad.Except (runExcept)
import DOM (DOM)
import Data.Array (filter, length, take)
import Data.Bifunctor (lmap)
import Data.Either (fromRight, isRight, Either(Right, Left))
import Data.Foreign (F, Foreign, ForeignError, readString)
import Data.Generic (class Generic, gShow)
import Data.Int (toNumber)
import Data.Maybe (Maybe(Nothing, Just))
import Data.Monoid (mempty)
import Data.String (singleton, toCharArray, toLower, uncons)
import Data.Traversable (traverse_, elem, sequence, traverse)
import Network.HTTP.Affjax (AJAX, Affjax, URL, get)
import Network.HTTP.Affjax.Response (class Respondable)
import Partial.Unsafe (unsafePartial)
import Text.Smolder.HTML (p, div, img, a, h1, body, link, title, meta, head, html)
import Text.Smolder.HTML.Attributes (className, src, rel, href, name, content, httpEquiv, charset, lang)
import Text.Smolder.Markup ((!), Markup, text)
import Text.Smolder.Renderer.String (render)
import Prelude hiding (div)

-- | Anime data type

newtype Anime = Anime
  { title :: String
  , image :: URL
  , url   :: URL
  }

derive instance genericAnime :: Generic Anime

instance showAnime :: Show Anime where
  show = gShow

readAnime :: String -> Foreign -> F Anime
readAnime s value = do
  t <- value FI.! "data" FI.! 0 FI.! "attributes" FI.! "titles" FI.! "en_jp" >>= readString
  u <- value FI.! "data" FI.! 0 FI.! "id" >>= readString
  i <- value FI.! "data" FI.! 0 FI.! "attributes" FI.! "posterImage" FI.! "small" >>= readString
  pure $ Anime { title: t, url: "https://kitsu.io/anime/" <> u, image: i }


findAnime :: forall e. String -> Aff ( ajax :: AJAX | e) (Either ForeignError Anime)
findAnime query = do
  res <- get ("https://kitsu.io/api/edge/anime/?filter[text]={" <> trimm query <> "}")
  pure $ lmap extract $ runExcept $ readAnime (trimm query) res.response
 where
   trimm = replaceSpaces <<< toLower <<< deleteStr ['!']

-- | Episode data type

newtype Episode = Episode
  { anime :: Anime
  , torrent :: URL
  , number :: Int
  , release :: String
  }

derive instance genericEpisode :: Generic Episode

instance showEpisode :: Show Episode where
  show = gShow


fromNyaa
  :: forall e
   . Int
  -> Aff
       ( ajax :: AJAX | e )
       (Either (Either String ForeignError) (Array Episode))
fromNyaa limit = do
  entries <- getAnimes :: forall e'. Affjax e' String
  map (sequence <<< filter isRight) (traverse itemToEpisode $ take limit $ parseItems entries.response)

getAnimes :: forall e a. (Respondable a) => Affjax e a
getAnimes = get "https://www.nyaa.se/?page=rss&cats=1_37&term=1080p&filter=2"

foreign import parseItems :: String -> Array { link :: String, name :: String }

itemToEpisode
  :: forall e
   . { link :: String, name :: String }
  -> Aff ( ajax :: AJAX | e ) (Either (Either P.ParseError ForeignError) Episode)
itemToEpisode item =
  let str = parseTitle $ replaceAll '_' ' ' item.name
  in case str of
    Left err -> pure (Left $ Left err)
    Right rs -> do
      a <- attempt $ findAnime rs.title
      case a of
        Right (Right (Anime anime))
          | similar 0.6 (toCharArray rs.title) (toCharArray anime.title) -> do
          pure $ pure $ Episode { anime: Anime anime, torrent: item.link, number: rs.ep, release: rs.release }
        _ ->
          pure $ pure $ Episode { anime: notFoundAnime item.name, torrent: item.link, number: -1, release: "" }


similar :: Number -> Array Char -> Array Char -> Boolean
similar limit str1 str2 =
  toNumber (length $ filter (_ `elem` str2) str1) / toNumber (length str1) > limit

notFoundAnime :: String -> Anime
notFoundAnime name = Anime
  { title : name
  , image : ""
  , url   : ""
  }


parseTitle :: String -> Either P.ParseError { release :: String, title :: String, ep :: Int }
parseTitle = P.parse do
    rel <- P.skipSpaces *> release
    P.skipSpaces
    t <- pTitle
    num <- P.int
    case t of
      Nothing  -> P.fail "Could not parse title"
      Just ttl -> pure { release: rel, title: ttl, ep: num }

release :: P.Parser P.ParseError String
release =
  P.fromCharList <$> P.brackets (P.many $ P.isn'tAny "[]")


pTitle :: P.Parser P.ParseError (Maybe String)
pTitle = do
  x <- map (const Nothing) (P.string " - ") <|> map Just P.item
  case x of
    Nothing -> pure Nothing
    Just c -> do
      rest <- pTitle
      case rest of
        Nothing -> pure (Just (singleton c))
        Just r -> pure (Just (singleton c <> r))




newtype Item = Item { link :: String, name :: String }

instance showItem :: Show Item where
  show (Item item) = "{ link: " <> item.link <> ", name: " <> item.name <> " }"

-- Glue

replaceSpaces :: String -> String
replaceSpaces = replaceAll ' ' '-'

replaceDashes :: String -> String
replaceDashes = replaceAll '-' ' '

replaceAll :: Char -> Char -> String -> String
replaceAll from to str =
  case uncons str of
    Just r ->  singleton (if r.head == from then to else r.head) <> replaceAll from to r.tail
    Nothing -> mempty

deleteStr :: Array Char -> String -> String
deleteStr except str =
  case uncons str of
    Just r -> (if r.head `elem` except then id else (singleton r.head <> _)) $ deleteStr except r.tail
    Nothing -> mempty

-- Rendering

doc :: forall a. Array Episode -> Markup a
doc eps = html ! lang "en" $ do
  head $ do
    meta ! charset "utf-8"
    meta ! httpEquiv "X-UA-Compatible" ! content "IE=edge,chrome=1"
    title $ text "Nyaaaaa~"
    meta ! name "viewport" ! content "width=device-width"
    link ! rel "stylesheet" ! href "style.css"
  body $ do
    div ! className "container" $ do
      a ! href "https://www.nyaa.se/?page=search&cats=1_37&filter=2&term=" $ h1 $ text "Nyaaaaa~"
      div ! className "episodes" $ (traverse_ docEp eps)

docEp :: forall a. Episode -> Markup a
docEp (Episode ep) =
  div ! className "episode" $ do
    (docAnime ep.number ep.anime)
    (a ! href ep.torrent $ text "torrent")

docAnime :: forall a. Int -> Anime -> Markup a
docAnime ep (Anime anime) =
  a ! className "anime" ! href anime.url $
    div $ do
      p (text anime.title)
      p (text ("Episode: " <> show ep))
      (img ! src anime.image)

-- main

main :: forall e.
  Eff
    ( exception :: EXCEPTION
    , ajax :: AJAX
    , console :: CONSOLE
--    , dom :: DOM
    | e
    )
    Unit
main = do
  void $ launchAff do
    episodes' <- fromNyaa 30
    let eps = unsafePartial (fromRight episodes')
    log $ render $ doc eps
    -- liftEff $ setHtml $ render $ doc eps

foreign import setHtml :: forall e. String -> Eff ( dom :: DOM | e ) Unit

