var cheerio = require('cheerio');

exports.parseItems = function(data) {
    var items = [];
    var $ = cheerio.load(data, { xmlMode: true });
    //console.log($("rss").find("item").length);
    $("rss").find("item").each(function (i, elem) {
        var el = $(this);
	items.push({ link: el.find("link").text()
		   , name: el.find("title").text()
		   });
    });
    return items;
};

exports.setHtml = function(html) {
    return function() {
	document.getElementsByTagName("html")[0].outerHTML = html;
    }
};
