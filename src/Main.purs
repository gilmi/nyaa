module Main where

import Animetime as A
import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE)
import Control.Monad.Eff.Exception (EXCEPTION)
-- import DOM (DOM)
import Network.HTTP.Affjax (AJAX)
import Prelude (Unit)

main :: forall e.               
  Eff                   
    ( ajax :: AJAX      
    , console :: CONSOLE
    , exception :: EXCEPTION
--    , dom :: DOM
    | e                 
    )                   
    Unit
main = A.main
